import React, { Component } from 'react';
import { Link } from '@reach/router'

import JumptCutLogo from '../images/jumpcut-logo.png'

class Navbar extends Component {
    render() {
        return (
            <nav className="nav-wrapper">
                <div className="container">
                    <Link to="/">
                        <img src={JumptCutLogo} className="brand-logo" />
                        <label className="project-name">Sequencers</label>
                    </Link>
                </div>
            </nav>
        )
    }
}

export default Navbar;