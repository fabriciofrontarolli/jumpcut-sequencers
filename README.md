# JUMPCUT sequencers


[![CircleCI](https://circleci.com/bb/fabriciofrontarolli/jumpcut-sequencers/tree/master.svg?style=svg)](https://circleci.com/bb/fabriciofrontarolli/jumpcut-sequencers/tree/master)


### Tech

JUMPCUT Sequencers uses a number of open source projects to work properly:

* [ReactJS] - As frontend lib!
* [Redux] - A Predictable State Container for JS Apps
* [ParcelJS] - A seamless packer to interpret SASS, JAX, ES6, etc...
* [Materialize] - great UI Framework for modern web apps

### Running the Project

JUMPCUT Sequencers requires [Node.js](https://nodejs.org/) v10+ to run.

Install the dependencies and devDependencies and start the server.

```sh
$ cd jumpcut
$ npm install
$ npm start
```

- The app will start at port 15000

### Test Coverage
To get the code coverage for the backend run:

```sh
$ cd jumpcut
$ npm install
$ npm run test:backend-coverage
```

![JUMPCUT Sequencers Coverage](https://i.imgur.com/5o0Nvuh.png)

### Demo

![](https://i.imgur.com/guy3S77.gif)


### Consideradions

### Workflow

- One single branch (master) (as for the test).
- I started with Test Driven Development on the Sequencers, Generator and PipedSequencer.
- Backend done, moved on to the frontend to design, integrate and develop the interface and functionality.

### CI/CD

- I setup CI with CircleCI. It has 2 workflows: 
--- *build*: This job build and packs the application to make it available for deployment
--- *test*: This job runs and displays test coverage for the tests in the application
- Continuous Delivery: I had some issues when setting up the deployment to Heroku. (Work in Progress).




### TODOs

- Backend (Lib)
     - Write MORE Tests
     - Properly document (JSDoc) each Sequencers, Generator, PIpedSequencer
 - Frontend
    - Make use of the Generators and Accumulators functions (make it available for the user)
    - Add PropTypes to each component
    - Write Tests for the components
    - Add Validations
    - Improve Layout
- CI/CD

License
----
MIT

**Free Software, Hell Yeah!**